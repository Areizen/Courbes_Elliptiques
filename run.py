from src.Point import Point
from src.EllipticCurve import EllipticCurve
from src.DiffieHellman import DiffieHellman
from src.ElGamal import ElGamal

'''
n = 0xF1FD178C0B3AD58F10126DE8CE42435B3961ADBCABC8CA6DE8FCF353D86E9C03
a = 0xF1FD178C0B3AD58F10126DE8CE42435B3961ADBCABC8CA6DE8FCF353D86E9C00
b = 0xEE353FCA5428A9300D4ABA754A44C00FDFEC0C9AE4B1A1803075ED967B7BB73F
p = Point(0xB6B3D4C356C139EB31183D4749D423958C27D2DCAF98B70164C97A2DD98F5CFF,0x6142E0F7C8B204911F9271F0F3ECEF8C2701C307E8E4C9E183115A1554062CFB)
'''

n = 17
a = 1
b = 3
p = Point(3, 4)

point = Point(2,8)


ec = EllipticCurve(a, b, p, n)

'''
df = DiffieHellman(ec)
df.keys_exchange()
'''


eg = ElGamal(ec)
eg.generate_private_and_public_key()

if(ec.is_point_on_the_curve(point)):
    c1,c2 = eg.cipher(point)

    m = eg.decipher(c1,c2)
