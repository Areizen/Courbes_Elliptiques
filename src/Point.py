# coding: utf-8
class Point :

    def __init__(self, a, b, c = 0):
        self.a = a
        """ a, coordonée x du point """
        self.b = b
        """ b, coordonée y du point """
        self.c = c
        """ c, indice qui défini si le point est à l'infini (0 = false)"""

    def inversePoint(self,n):
        """
        Permet de transformer un point P en -P
        """
        self.b = -self.b % n
