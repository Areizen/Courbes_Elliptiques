# coding: utf-8

from src.Point import Point

class EllipticCurve:

    def __init__(self,coef_a,coef_b,point,cardinal):
        self.coef_a = coef_a
        """ coef_a, coefficient a de l'equation y² = x³ + ax +b"""
        self.coef_b = coef_b
        """ coef_b, coefficient b de l'equation y² = x³ + ax +b"""
        self.point = point
        """ point, point generateur """
        self. cardinal = cardinal
        """ cardinal, cardinal de la courbe elliptique"""


    def is_point_on_the_curve(self,p):
        """
        Verifie si un point est sur la courbe
        @param p: point à vérifier
        @return: True si le point est sur la courbe sinon False
        """
        y = p.b
        x = p.a
        a = self.coef_a
        b = self.coef_b
        card = self.cardinal

        """
            On effectue l'équation : y² = x³ + ax + b pour
            savoir si le point appartient à la courbe
        """
        return ( pow(y,2,card) == ( ( pow(x,3) + a*x + b ) % card ) )


    def extended_gcd(self,aa, bb):
        """
        Permet d'utiliser l'algorithme d'euclide étendu
        @param aa: valeur a
        @param bb: valeur b
        @return plein de trucs
        """
        lastremainder, remainder = abs(aa), abs(bb)
        x, lastx, y, lasty = 0, 1, 1, 0
        while remainder:
            lastremainder, (quotient, remainder) = remainder, divmod(lastremainder, remainder)
            x, lastx = lastx - quotient*x, x
            y, lasty = lasty - quotient*y, y
        return lastremainder, lastx * (-1 if aa < 0 else 1), lasty * (-1 if bb < 0 else 1)


    def modinv(self,x):
        """
        Permet de calculer l'inverse modulaire d'un point
        @param x: entier à inverser
        @return: inverse modulaire de x
        """
        card = self.cardinal
        g, a, y = self.extended_gcd(x, card)
        if g!=1:
            raise ValueError
        return a % card



    def sum_same_point(self, p1):
        """
        Permet de calculer la somme de deux points égaux
        @param p1: point à multiplier par 2
        @return: résultat de la multiplication par 2
        """
        card = self.cardinal
        x = p1.a
        y = p1.b
        x_result = 0
        y_result = 0

        """
        On traite le cas ou 2P = Point à l'infini
        """
        if( y == 0 ):
            p1.c = 1
            return p1

        if ( p1.c == 1):
            return p1

        _lambda = ( ( 3*pow(x,2) + self.coef_a ) * self.modinv( 2*y ) % card )

        x_result = ( pow(_lambda,2) - 2*x ) % card
        y_result = (_lambda * ( x - x_result ) - y ) % card

        return Point(x_result, y_result)


    def sum_different_points(self,p1,p2):
        """
        Permet de calculer le point résultant de la somme de deux différents
        @param p1: premier Point
        @param p2: second Point
        @return: resultat de l'addition des deux points
        """
        card = self.cardinal
        x1 = p1.a
        x2 = p2.a
        y1 = p1.b
        y2 = p2.b
        x_result = 0
        y_result = 0

        """
        On traite le cas ou P + Q = Point à l'infini
        """
        #print(f"Point 1: {x1,y1,p1.c} ;Point 2: {x2,y2,p2.c}")
        if( x1 == x2 ):
            p1.c = 1
            return p1

        if( p1.c != 0 ):
            return p2

        if( p2.c != 0 ):
            return p1


        _lambda = ( ( y2 - y1 ) * self.modinv( x2 - x1 ) ) % card
        x_result = (pow(_lambda,2) - x1 - x2 ) % card
        y_result = (_lambda * ( x1 - x_result ) - y1 ) % card

        return Point(x_result, y_result)



    def sum_two_points(self,p1,p2):
        """
        Permet de calculer le point résultant de la somme de deux autres
        @param p1: premier Point
        @param p2: second Point
        @return: resultat de l'addition des deux points
        """
        x1 = p1.a % self.cardinal
        x2 = p2.a % self.cardinal
        y1 = p1.b % self.cardinal
        y2 = p2.b % self.cardinal

        if( x1 == x2 and y1 == y2 ):
            return self.sum_same_point(p1)
        else :
            return self.sum_different_points(p1,p2)


    def fast_exponentiation(self,factor,point):
        """
        Permet de faire une multiplication scalaire
        @param factor: facteur de la multiplication scalaire
        @return: resultat de la multiplication scalaire
        """
        res = Point(0,0,1)
        while(  0 < factor):
            if factor & 1:
                res = self.sum_two_points(res,point)
            factor >>= 1
            point = self.sum_two_points(point,point)
        return res
