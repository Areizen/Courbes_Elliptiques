# coding: utf-8
import secrets

class DiffieHellman :

    def __init__(self,ec):
        self.ec = ec
        """ ec, Courbe Elliptique """


    def keys_exchange(self):
        '''
        Simule un échange de clés DiffieHellman
        '''
        print("------------------ Géneration ------------------")
        print("Génération de la clée de Alice :")
        randomA = secrets.randbelow(self.ec.cardinal - 1)
        #randomA = 2
        print(f"Clé d'Alice = {randomA}")

        print("Génération de la clée de Bob :")
        randomB = secrets.randbelow(self.ec.cardinal - 1)
        #randomB = 2
        print(f"Clé de Bob  = {randomB}")

        print("------------------ Echange ------------------")
        print("Envoie des secrets :")
        pA = self.ec.fast_exponentiation(randomA,self.ec.point)
        print(f"\tAlice : {pA.a,pA.b}")
        pB = self.ec.fast_exponentiation(randomB,self.ec.point)
        print(f"\tBob   : {pB.a,pB.b}")

        print("------------------ Calcul secret ------------------")
        kA = self.ec.fast_exponentiation(randomA,pB)
        kB = self.ec.fast_exponentiation(randomB,pA)
        print("Clé secrète commune :")
        print(f"\tAlice : {kA.a,kA.b}")
        print(f"\tBob   : {kB.a,kB.b}")
