# coding: utf-8
import secrets

class ElGamal :

    def __init__(self,ec):
        self.ec = ec
        """ ec, Courbe elliptique """


    def generate_private_and_public_key(self):
        '''
        Génère les clés publique et privé
        '''
        print(f"----------- Génération des clés ------------")
        print("Génération de la clé privée d'Alice")
        self.private = 1 + secrets.randbelow(self.ec.cardinal - 2)
        #self.private = 9
        print(f"Clé privée : {self.private}")

        print("Génération de la clé publique d'Alice")
        self.public = self.ec.fast_exponentiation(self.private, self.ec.point )
        print(f"Clé publique : {self.public.a,self.public.b}\n\n")


    def cipher(self,m):
        '''
        Chiffre un point M
        @param m: point a chiffrer
        @return: points correspondant au message chiffré (c1,c2)
        '''
        print(f"----------- Chiffrement de {m.a,m.b}-----------")
        print(f"Génération de k :")
        k = 1 + secrets.randbelow(self.ec.cardinal - 1)
        print(f"k : {k}")
        print("Chiffrement : ")
        c1 = self.ec.fast_exponentiation(k,self.ec.point)
        c2 = self.ec.sum_two_points( m, self.ec.fast_exponentiation(k, self.public))
        print(f"C1 : ({c1.a,c1.b})")
        print(f"C2 : ({c2.a,c2.b})\n\n")
        return c1,c2

    def decipher(self,c1,c2):
        '''
        Déchiffre un message chiffré
        @param c1: point numéro 1 du message chiffré
        @param c2: point numéro 2 du message chiffré
        @return: point déchiffré
        '''
        print(f"----------- Déchiffrement ------------")
        kA = self.ec.fast_exponentiation( self.private, c1)
        print(f"kA = ({kA.a,kA.b})")
        kA.inversePoint(self.ec.cardinal)
        m = self.ec.sum_two_points(c2,kA)
        print(f"M = ({m.a,m.b})")
        return m
