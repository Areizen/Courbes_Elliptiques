# Courbes Elliptiques

## Présentation :

Ce projet à pour but d'implémenter les courbes elliptiques en python
suite au cours portant sur celles-ci en 1 ère année d'Ensibs Cyberdéfense.

## Structure

Dans le fichier `run.py` un exemple de l'utilisation des différentes classes avec
une courbe recommendée par l'Anssi.

Dans le dossier `src/` :

  + L'implémentation de la courbe en elle-même : `EllipticCurve.py`

  + L'implémentation d'un chiffrement asymétrique par El-Gamal : `ElGamal.py`

  + L'implémentation d'un échange de clés via Diffie-Hellman : `DiffieHellman.py`

## Auteur :

Romain KRAFT <romain.kraft@protonmail.com>
